   /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */
 
  var btns = document.getElementsByClassName("showButton");
  var tabDiv = document.getElementById("tabContainer");
  var tabs = tabDiv.getElementsByClassName("tab");
  var btnCont = document.getElementById("buttonContainer");
  var closeEl = document.createElement("button");

  Array.from(btns).forEach ( function (item) {
      item.onclick  = function (){
        var dataValue = item.dataset.tab;
        console.log(dataValue);
        Array.from(tabs).forEach (function (itemTab) {
           
           if (dataValue == itemTab.dataset.tab) {
            itemTab.classList.toggle("active");
           }
          
        })
        var activeTabs = tabDiv.getElementsByClassName("active");
        if (activeTabs.length == 3){
          btnClose = btnCont.appendChild(closeEl);
          btnClose.innerText = "hide All Tabs";
          btnClose.id = "closeBut";
          btnClose.onclick = function (){
            hideAll();
          }
        }
        
      }
    });
  // Функция hideAllTabs
  function hideAll (){
    Array.from(tabs).forEach (function (itemTab) {
      itemTab.classList.remove("active");      
    })
    btnClose = document.getElementById("closeBut");
    btnClose.remove();
  }
