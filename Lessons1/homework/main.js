/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 6-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 256;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/


 // var bodyEl = document.getElementsByTagName("body");

      

function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    };

	// определяем рандомно RGB цвет
  var colorR = getRandomIntInclusive(0,256);
  var colorG = getRandomIntInclusive(0,256);
  var colorB = getRandomIntInclusive(0,256);

	//console.log(colorR + " " + colorG + " " + colorB);

  // задаем цвет фону
  var color = "#" + colorR + "" + colorG + "" + colorB;
	document.body.style.background = color;

  // пересчитываем цвет в HEX
	var colorR16 = colorR.toString(16);
	var colorG16 = colorG.toString(16);
	var colorB16 = colorB.toString(16);
  var color16 = colorR16 + " " + colorG16 + " " + colorB16;

	//console.log(colorR16 + " " + colorG16 + " " + colorB16);

  //выводим код цвета на экран
  var div = document.getElementById("app");
  var textElem = document.createTextNode(color16);
  div.appendChild(textElem);

  //центрируем код цвета
  div.style.position = "absolute";
  div.style.top = "50%";
div.style.left = "50%";
  //div.style.marginLeft = "45%";



