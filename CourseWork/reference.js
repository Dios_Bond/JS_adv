 
let typeMaterial = ["полиграфия", "рекламные материалы", "хоз.товары"];
let carriers = ["Новая Почта","Интайм","Деливери","Укрпочта"];
let marketingMaterial = [
		{
            type: "полиграфия",
            name: "Маленький принц",
            weight: 240,
            rest: 100
        },
        {
            type: "полиграфия",
            name: "Буклет",
            weight: 20,
            rest: 100
        },
        {
            type: "рекламные материалы",
            name: "стойка",
            weight: 12020,
            rest: 10
        },
        {
            type: "рекламные материалы",
            name: "карман",
            weight: 800,
            rest: 20
        },
         {
            type: "рекламные материалы",
            name: "баннер",
            weight: 3500,
            rest: 3
        }
        ];



let carriersDetail = [
        {
        	name: "Новая Почта",
        	city: "Авиаторское",
            weight: 240,
            department: "Отделение №1",
            adress: "ул. Аэродром, д.10, пом.91"
        },
        {
            name: "Новая Почта",
        	city: "Барышевка",
            weight: 240,
            department: "Отделение №1",
            adress: "ул. Центральная, 69 (маг. Павлуша)"
          },
          {
            name: "Новая Почта",
        	city: "Вишневое",
            weight: 240,
            department: "Отделение №1",
            adress: "ул. Киевская, 27"
          },
          {
            name: "Новая Почта",
        	city: "Вишневое",
            weight: 240,
            department: "Отделение №2",
            adress: "ул. Леси Украинки, 19"
          },
          {
            name: "Новая Почта",
        	city: "Вишневое",
            weight: 240,
            department: "Отделение №3",
            adress: "ул. Соборная, 21а, (Софиевская Борщаговка)"
          },
          {
            name: "Новая Почта",
        	city: "Вишневое",
            weight: 240,
            department: "Отделение №4",
            adress: "с. Софиевская Борщаговка, ул. Волошковая, 42/2"
          },
          {
         	name: "Новая Почта",
        	city: "Киев",
            weight: 240,
            department: "Отделение №1",
            adress: "ул. Пироговский путь, 135"
        }
        ];
	

