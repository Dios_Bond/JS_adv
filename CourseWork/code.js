
    let toList = document.getElementById("toList");
    let listItem = document.getElementById("listItem");

    var closeBtn = document.getElementById("popup__close");
    var popupDiv = document.getElementById("popup");
    var itemDiv = document.getElementById("popup__containe");
    var popupBtns = document.getElementsByClassName("popup__button");
    var popupItem = document.getElementsByClassName("popup__item");
    let signInBtn = document.getElementById("signIn");

    let logInp = document.getElementById("login");
    let passInp = document.getElementById("pass");
    let remBox = document.getElementById("agree");
    let formElements = document.forms.ValidateForm.elements;
    let addMatDiv = document.getElementById("add_material");
    let addMatNameDiv = document.getElementById("add_material_name");
    let divCarriers = document.getElementsByName("divCarriers");
    let materialNumbers = document.getElementById("add_material_numbers");
    let addOrder = document.getElementById("addOrder");
    let dateEl = document.getElementById("DateSelector");
    let ul = document.getElementById("toList");
    
    let user = localStorage.getItem("user");
    let pass = localStorage.getItem("password");
    if (user == "null") {user = ""}
      else {logInp.value = user};
    if (pass == "null") {pass = ""}
      else {passInp.value = pass};
    let orderArr = [];
    let count = 0;

    //блокируем кнопку добавления заказа
    addOrder.setAttribute("disabled", true);
   
  // авторизация 
    signInBtn.onclick = function (event){
        event.preventDefault();

        let myArrayColl = Array.from(document.forms.ValidateForm.elements);

            if (logInp.value == "" || passInp.value == ""){
                addError();
            }
              else {
                usersArr.forEach( function(item){
                  if (item.login == logInp.value){
                    if(item.pass == passInp.value){
                              
                      if (remBox.checked == true ){
                        auth(true)
                      }
                      else {
                        auth()
                      } 
                      //console.log(item)
                    }
                    else addError();                   
                  }
                  else addError();
                })
              }       
        };

    //основная загрузка инф на странице
    function loadFunc(){

        addSelect(addMatDiv, typeMaterial, "selectType");
        let selectTypeEl = document.getElementById("selectType");
        selectTypeEl.addEventListener("click", function() {
          addMatNameDiv.innerHTML = "";
           //очистка предыдущих значений
            removeAllChild(divCarriers[1]);
            removeAllChild(divCarriers[2]);
            removeAllChild(divCarriers[3]);
            removeAllChild(divCarriers[0]);
          let selectType = selectTypeEl.options[selectTypeEl.selectedIndex].text;
          console.log(selectType);

          let selectNameMaterial = [];
          let widthMaterial;
          marketingMaterial.forEach(function (item, i){
            if (item.type == selectType){
              selectNameMaterial[i]=item.name;
              weightMaterial = item.weight
            }
          });
          addSelect(addMatNameDiv, selectNameMaterial, "selectNameMaterial");
          let selectNameMaterialEl = document.getElementById("selectNameMaterial");
          let selectedNameMaterial = selectNameMaterialEl.options[selectNameMaterialEl.selectedIndex].text;
          addSelect(divCarriers[0], carriers, "selectCarriers");
          let selectCarriersEl = document.getElementById("selectCarriers");

          // работа с полями компании по доставке
          selectCarriersEl.addEventListener("click", function() {
            //очистка предыдущих значений
            
            removeAllChild(divCarriers[1]);
            removeAllChild(divCarriers[2]);
            removeAllChild(divCarriers[3]);

          //addMatNameDiv.innerHTML = "";
            let selectCarriers = selectCarriersEl.options[selectCarriersEl.selectedIndex].text;

            //селект городов для доставки
            let сarriersCity = [];
            carriersDetail.forEach(function (item, i){
              if (item.name == selectCarriers){
                сarriersCity[i]=item.city
              }
            });
            addSelect(divCarriers[1], сarriersCity.filter(uniqueVal), "selectCarriersCity");


            //селект адреса для доставки
            let selectCarriersCityEl = document.getElementById("selectCarriersCity");
            selectCarriersCityEl.addEventListener("click", function() {
                      //очистка предыдущих значений
                removeAllChild(divCarriers[2]);
                removeAllChild(divCarriers[3]);

                let selectCarriersCity = selectCarriersCityEl.options[selectCarriersCityEl.selectedIndex].text;
                //селект отделения
                let departmentCity = [];
                  carriersDetail.forEach(function (item, i){
                    if (item.name == selectCarriers){
                      if(item.city == selectCarriersCity)
                      departmentCity[i]=item.department
                    }
                });
                addSelect(divCarriers[2], departmentCity, "selectCarriersDepartment");
                
                let carriersDepartmentEl = document.getElementById("selectCarriersDepartment");
                  carriersDepartmentEl.addEventListener("click", function() {
                    //очистка предыдущих значений
                    removeAllChild(divCarriers[3]);

                    //разблокировка кнопки добавления
                    addOrder.removeAttribute("disabled");

                    //отделение перевозчика и информация о адресе
                    let selectCarriersDepartment = carriersDepartmentEl.options[carriersDepartmentEl.selectedIndex].text;
                    let adressCarriersDepartment;
                    let carriersWeight;
                    carriersDetail.forEach(function (item, i){
                      if (item.name == selectCarriers){
                        if(item.city == selectCarriersCity)
                          if(item.department == selectCarriersDepartment){
                            adressCarriersDepartment = item.adress;
                            carriersWeight = item.weight;
                            if (materialNumbers.value != ""){
                              console.log(materialNumbers.value)
                              console.log(materialNumbers)
                              console.log(weightMaterial)
                              console.log(carriersWeight)
                              if((materialNumbers.value * weightMaterial) > carriersWeight){
                                let AlertEl = document.createElement("div");
                                AlertEl.innerHTML ="Внимание! данное отделение не может принять такой вес";
                                divCarriers[3].appendChild(AlertEl);
                                AlertEl.style.color = "red";
                              }
                            }
                          }
                        }

                    });
                    let carriersAdressEl = document.createTextNode(adressCarriersDepartment);
                    divCarriers[3].appendChild(carriersAdressEl);
                    let carriersDate = dateEl.value;
                    addOrder.addEventListener("click", function() {

                      orderArr[count] = new Order(user, selectType, selectNameMaterial,materialNumbers.value, carriersDate);
                      count ++;
                          //очистка предыдущих значений
                          removeAllChild(divCarriers[1]);
                          removeAllChild(divCarriers[2]);
                          removeAllChild(divCarriers[3]);
                          removeAllChild(divCarriers[0]);
                          addMatNameDiv.innerHTML = "";

                          //сбрасываем тип материала в селекте
                          selectTypeEl.options[0].selected = true;




                          //блокируем кнопку добавления
                          addOrder.setAttribute("disabled", true);

                      console.log(orderArr);
                      orderList();
                      
                                                    
                       })
                   
                  })
          
              })
          })
        });
    }


//подсветка эроров авторизации
    function addError(){
      formElements["login"].classList.add("error");
      formElements["pass"].classList.add("error");
    };
//авторизация
    function auth(p){
      user = logInp.value;
      localStorage.setItem("user", user);
      if (p == true){
        pass = passInp.value;
        localStorage.setItem("password", pass)
      }
      var authEl = itemDiv.getElementsByClassName("show");
      authEl[0].classList.remove("show");
      popupDiv.classList.remove("popup");
      popupDiv.classList.add("opacity");
      //получаем сохраненные заказы
      readData = localStorage.getItem("data");
      console.log(readData);
      if (readData != null){
        orderArr = JSON.parse(readData);
        count = orderArr.length;
        orderList();
      }
      loadFunc();
    }
//добавление селектов
   function addSelect (parent, arr, idElement){
      let selectElement = document.createElement("select");
          parent.appendChild(selectElement);
          selectElement.id = idElement;
          var optDef = new Option("", "", null, true);
          selectElement.appendChild(optDef);
          optDef.setAttribute("disabled", true)
              arr.forEach( function(item, key){
                var option;
                option = new Option( item, name);
                selectElement.appendChild(option);
              });
    }
    //отбор уникальных значейний
    function uniqueVal(value, index, self){
      return self.indexOf(value) === index;
    }

    //конструктор обьекта заказа
    function Order(user, typeMaterial, nameMaterial, quantity, dateMaterial, comment, carriers, carriersCity, carriersDepartment, carriersDepartment, carriersAdress){
      this.user = user;
      this.typeMaterial = typeMaterial;
      this.nameMaterial = nameMaterial;
      this.quantity = quantity;
      this.dateMaterial = dateMaterial;
      this.comment = comment;
      this.carriers = carriers;
      this.carriersCity = carriersCity;
      this.carriersDepartment = carriersDepartment;
      this.carriersAdress = carriersAdress;
      this.status = "новый заказ";
      this.ttn =""
    };

    function removeAllChild(element){
      while (element.firstChild){
        element.removeChild(element.firstChild);
      }
    };

    //Функция сборки перечня заказов
    function orderList(){
      removeAllChild(ul);
      orderArr.forEach(function (item, i){
        addLi(item.dateMaterial, item.typeMaterial, i, item.status);
      });
      let Jdata = JSON.stringify(orderArr);
      console.log("Jdata", Jdata);
      localStorage.setItem("data", Jdata);
    }


    //перебираем, при нажатии на кнопку добавляем классы
    Array.from(popupBtns).forEach ( function (item) {
      item.onclick  = function (){
        var num = item.dataset.popup;
        popupItem[num-1].classList.add("show");
        popupDiv.classList.add("popup");
        
    }
  })

    //элемент заказа в перечне
    function addLi(date, txtV, idNumber, status){
        var li = document.createElement("li");
        var span = document.createElement("span");
        var btnR = document.createElement("button");
        toList.appendChild(li);
        li.id = idNumber;
        li.appendChild(span);
        li.appendChild(btnR);        
        span.innerText = date + " " + txtV + " (Статус - " + status + ")";
        if(status == "выполнено"){
           span.classList.add("done");
           btnR.setAttribute("disabled", true);
        }       
        btnR.innerText = "Remove";
        btnR.style.color = "red";
        btnR.id = idNumber;      
    }

    toList.addEventListener("click", function() {
      if (event.target.innerText == "Remove"){
          //console.log(event.target.id + " кнопка +");
          //CommentsArray[event.target.id-1].plusLike();
          RemoveElement(event.target.id-1)
      };    
    });

    //удаление заказов
    function RemoveElement(id){
      orderArr.splice(id, 1)
      orderList();
     }
