/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

  var OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
  var currentPosition = 0;

var slider = document.getElementById("slider");
var btn = document.getElementById("SliderControls");
var btns = btn.getElementsByTagName("button");
var prevSilde = document.getElementById("PrevSilde");
var nextSilde = document.getElementById("NextSilde");


//событие load обьекта window
window.onload = function(){
  addImg(OurSliderImages[currentPosition]);
}

prevSilde.classList.add("buttonAnimated");
nextSilde.classList.add("buttonAnimated");


//навигация назад по слайдеру
btns[0].addEventListener("click", function () {
  deleteChild(slider);
  if (currentPosition > 0){
    currentPosition = currentPosition-1;
    addImg(OurSliderImages[currentPosition]);
  }
  else {
    currentPosition = 7;
    addImg(OurSliderImages[currentPosition]);
  }

  console.log(currentPosition);
  console.log(OurSliderImages[currentPosition]);
})

//навигация вперед по слайдеру
btns[1].addEventListener("click", function () {
  deleteChild(slider);
  if (currentPosition < 7){
    currentPosition = currentPosition+1;
    addImg(OurSliderImages[currentPosition]);  
  }
  else {
    currentPosition = 0;
    addImg(OurSliderImages[currentPosition]);
    
  }
  console.log(currentPosition);
  console.log(OurSliderImages[currentPosition]);

})
//btns.addEventListener("click", function(){
  //btn.target.getElementsByTagName("button")

  console.log(btns)
//})

function addImg(arr){
  //deleteChild(result);
  let imgEl = document.createElement("img");
  imgEl.setAttribute("src", arr);
  imgEl.setAttribute("width", "300px"); 
  sliderChild = slider.appendChild(imgEl);
  //console.log(OurSliderImages);
  sliderChild.classList.add("loadedImage");
  //buttonAnimated
}

function deleteChild(myNode) {
  while (myNode.firstChild) {
    myNode.removeChild(myNode.firstChild);
  }
}