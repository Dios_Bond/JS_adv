/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/
let addBtn = document.getElementById("addComment");
let toList = document.getElementById("toList");
let listItem = document.getElementById("listItem");
let inputName = document.getElementById("nameComment");
let inputText = document.getElementById("input");
// Url в ДОМ не выводим, используем везьде по умолчанию

let ComDefault = {
    url: "DefaultPictures.jpg"
};

var CommentsArray = [];

function Comment(name, text, url){
        this.name = name;
        this.text = text;
        this.url = url;
        this.likes = 0;
        this.Prototype = ComDefault;
      
        this.plusLike = () => {
            this.likes += 1;
            console.log(`Comment ${this.name} likes= ${this.likes}`);
        };
        this.minusLike = () => {
            this.likes -= 1;
            console.log(`Comment ${this.name} likes= ${this.likes}`);
        };
        
        console.log('create comment ', this);
    };

    addBtn.addEventListener("click", function() {
      
      CommentsArray[CommentsArray.length] = new Comment(inputName.value, inputText.value, "url://bla.jpg");
      console.log(CommentsArray[CommentsArray.length-1].name);
      addLi(CommentsArray[CommentsArray.length-1].name, CommentsArray.length);
      
    });


function addLi(txtV, idNumber){
        var li = document.createElement("li");
        var span = document.createElement("span");
        var btnP = document.createElement("button");
        var btnM = document.createElement("button");
        var spanLike = document.createElement("span");
        toList.appendChild(li);
        li.id = idNumber;
        li.appendChild(span);
        li.appendChild(btnP);
        li.appendChild(btnM);
        li.appendChild(spanLike);
        span.innerText = txtV;
        spanLike.classList.add("like");
        btnP.innerText = "+";
        btnP.id = idNumber;
        btnP.classList.add("plus");
        btnM.innerText = "-";
        btnM.id = idNumber;
        btnM.classList.add("minus");
        spanLike.innerText = "0";
      }

    toList.addEventListener("click", function() {
      if (event.target.innerText == "+"){
          //console.log(event.target.id + " кнопка +");
          CommentsArray[event.target.id-1].plusLike();
          let listLike = Array.from(document.getElementsByClassName("like"));
          //console.log("like this elem = " + CommentsArray[event.target.id].likes);
          listLike[event.target.id-1].innerText = CommentsArray[event.target.id-1].likes;
      };
      if (event.target.innerText == "-"){
          //console.log(event.target.id + " кнопка -");
          CommentsArray[event.target.id-1].minusLike();
          let listLike = Array.from(document.getElementsByClassName("like"));
          //console.log("like this elem = " + CommentsArray[event.target.id].likes);
          listLike[event.target.id-1].innerText = CommentsArray[event.target.id-1].likes;
      };
    });
     console.log(CommentsArray.length);
    //let comment1 = new Comment("Name", "Text", "url://bla.jpg");
    //comment1.plusLike();
    //comment1.plusLike();
    //comment1.minusLike();
   
    //console.log(CommentsArray[1]);