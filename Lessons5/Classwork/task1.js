/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/

    const TRAIN = {};
    TRAIN.name = "bla-bla-train";
    TRAIN.speed = 90;
    TRAIN.passenger = 15;

    console.log(TRAIN);

    TRAIN.go = function(){
        this.speed = 100;
        console.log(this.name + " везет " + this.passenger + " пасажиров со скростью " + this.speed)
    }
    
    TRAIN.stay = function(){
        this.speed = 0;
        console.log( this.name + " остановился" + 'Speed' + this.speed + 'km/h')
    }
    
    TRAIN.addPas = function( newPass ){
        this.passenger += Number(newPass);
        console.log(this.name + " увеличивает количество пассажиров до " + this.passenger)
    }
    
    
    TRAIN.go();
    TRAIN.addPas( '130' );
    TRAIN.stay();
    TRAIN.go();

