/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.

    Например:
      encryptCesar('Word', 3);
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1('Sdwq', 3);
      decryptCesar1(...)
      ...
      decryptCesar5(...)

*/

    //let offset = 3;
    //let phrase = "А а Б б";
    //let codephrase = "";



    function encryptCesar(phrase, offset){
      let codephrase = "";
      for (let i = 0; i < phrase.length; i++){
        let code = phrase.charCodeAt(i);
        code = code + offset;
        codephrase += String.fromCharCode(code);
      };
      console.log("шифрованная фраза " + phrase + " = " + codephrase)
    };

    function decryptCesar(phrase, offset){
      let codephrase = "";
      for (let i = 0; i < phrase.length; i++){
        let code = phrase.charCodeAt(i);
        code = code - offset;
        codephrase += String.fromCharCode(code);
      };
      console.log("дэшифрованная фраза " + phrase + " = " + codephrase)
    };

    let text = "bla bla car";
    function encryptCesar1(text){
      return encryptCesar.bind(null, text, 1)();
    }
    function encryptCesar2(text){
      return encryptCesar.bind(null, text, 2)();
    }
    function encryptCesar3(text){
      return encryptCesar.bind(null, text, 3)();
    }


    encryptCesar(text, 3);
    decryptCesar("Г#г#Д#д", 3);
    encryptCesar1(text);
    encryptCesar2(text);
    encryptCesar3(text);



    
        