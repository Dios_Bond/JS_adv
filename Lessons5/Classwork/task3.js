/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

*/

    function Dog(name, poroda){
        this.name = name;
        this.poroda = poroda;
        
        this.status = 'idle';
        this.run = () => {
            this.status = 'run';
            console.log(`${this.name} running`);
        };
        this.eat = () => {
            this.status = 'eat';
            console.log(`${this.name} eat`);
        };
        this.idle = () => {
            this.status = 'idle';
            console.log(`${this.name} idle`);
        };
        console.log('created dog', this);
    }
    /*
    function addStatus(stat){
        myObj.status = stat;
        console.log(myObj);
    }
    */

    let Djesja = new Dog("Djesja", "Rottweiler");
    Djesja.run();
    Djesja.idle();
    Djesja.eat();
    console.log(Djesja);

    // Функция перебора свойств обьекта
    for (key in Djesja) {
      console.log( "свойство " + key + " = " + Djesja[key] );
    }
    