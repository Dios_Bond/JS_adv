/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();

*/
  let colors = {
    background: 'purple',
    color: 'green'
  }
    var h1 = document.createElement("h1");
    var div = document.getElementById("div");
    div.appendChild(h1);


    function myCall( back ){
        document.body.style.background = back;
        document.body.style.color = this.color;
    }
    // myCall.call( colors, 'red' );

     function myBind( ){
        document.body.style.background = this.background;
        document.body.style.color = this.color;
    }
   
    
    var BindFunc = myBind.bind(colors);
    //BindFunc();


    function myApply( arg ){
        document.body.style.background = this.back;
        document.body.style.color = this.color;
        console.log(arg)
        var text = String(arg);
        h1.innerHTML = String(arg);
    }
    var arg = ['Ij know how binding works in JS'];
    myApply.apply(colors, arg );
